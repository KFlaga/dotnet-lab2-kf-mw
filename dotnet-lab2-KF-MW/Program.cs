﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameoflifekfmw
{
    class Program
    {
        static Board board;

        static void Main(string[] args)
        {
            board = new Board();
            board.Generate(30);
            board.Show();

            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                if (key.KeyChar == 'q')
                    break;
                board.Update();
                Console.Clear();
                board.Show();
            }
        }
    }

    class Board
    {
        bool[,] board;

        public Board()
        {
            board = new bool[10, 10];
        }

        public void Update()
        {
            bool[,] locBoard = new bool[10, 10];
            Array.Copy(board, locBoard, 100);
            for (int r = 0; r < 10; r++)
            {
                for (int c = 0; c < 10; c++)
                {
                    int neighboors = 0;
                    for (int rn = Math.Max(0, r - 1); rn < Math.Min(10, r + 2); rn++)
                    {
                        for (int cn = Math.Max(0, c - 1); cn < Math.Min(10, c + 2); cn++)
                        {
                            if (board[rn, cn] == true && !(rn == r && cn == c))
                            {
                                neighboors++;
                            }
                        }
                    }

                    if (neighboors == 3 && board[r, c] == false)
                        locBoard[r, c] = true;
                    else if ((neighboors == 2 || neighboors == 3) && board[r, c] == true)
                        locBoard[r, c] = true;
                    else
                        locBoard[r, c] = false;
                }
            }

            Array.Copy(locBoard, board, 100);
        }

        public void Generate(int aliveCount)
        {
            while (true)
            {
                Random rand = new Random();
                int r = rand.Next(0, 10);
                int c = rand.Next(0, 10);

                if (board[r, c])
                    continue;
                board[r, c] = true;
                if (--aliveCount == 0)
                    break;

            }
        }

        public void Show()
        {
            for (int r = 0; r < 10; r++)
            {
                for (int c = 0; c < 10; c++)
                {
                    if (board[r, c] == false)
                        Console.Write('.');
                    else
                        Console.Write('*');
                }
                Console.Write('\n');
            }
        }
    }
}
